# Description

A simple API application repository as an initial requirement for submitting an application at Jabar Digital Services.


## Requirements
* PHP 5.6 >< PHP 8.0

## Features

As per the specified requirements, this application has 3 endpoints for each of the following functions:

* Registering a new account with a password generated automatically by the backend API
* A login function that will generate an access token for validation of each request
* Validate tokens to see which users have the right to use the token

All available endpoints can be viewed and tested on Postman using this file [JDC Test Simple API.postman_collection.json](https://gitlab.com/abduranggasenjari/jdc_rangga/-/blob/main/JDC%20Test%20Simple%20API.postman_collection.json)

## Installation
Follow this guide to install and use this API.

### Deploy on webserver
Clone this repository to your web directory
```bash
$ git clone https://gitlab.com/abduranggasenjari/jdc_rangga.git
```
Edit the `.env` file and adjust your MySQL parameters
```sh
DB_HOSTNAME=<database IP/hostname>
DB_USERNAME=<database username>
DB_PASSWORD=<database password>
DB_DATABASE=<database name>
DB_DRIVER=mysqli
```
Your API can be accessed on your webserver address
```bash
http://your_address
```
### Deploy on Docker
This repository is also available as an image on the Docker Hub at [ranggasenjari/jdc_api_rangga](https://hub.docker.com/repository/docker/ranggasenjari/jdc_api_rangga)

Clone this repository to a directory on your Docker host
```bash
$ git clone https://gitlab.com/abduranggasenjari/jdc_rangga.git
```
Edit `docker-compose.yaml` file and adjust http port
```sh
. . .
    ports:
      - "<your_port>:80"
. . .
```
Run `docker-compose up -d` on the application directory
```bash
$ cd jdc_rangga
$ docker-compose up -d
```
Your API is accessible on your Docker address
```bash
http://your_address:your_port
```
## Usage

Gunakan ketentuan berikut untuk menggunakan API ini

| Method | Endpoint | Request | Response |
| ------ | ------ | ------ | ------ |
| POST | /api/register | [BODY] <br> - username <br> - role| [JSON] <br> - username <br> - role <br> - password
| POST | /api/login | [BODY] <br> - username <br> - password| [JSON] <br> - id <br> - username <br> - accessToken
| GET | /api/app/validasi | [HEADER] <br> - Authorization : Bearer < token > | [JSON] <br> - is_valid <br> - expired_at <br> - username

## API Test
You can use Postman and then import the [JDC Test Simple API.postman_collection.json](https://gitlab.com/abduranggasenjari/jdc_rangga/-/blob/main/JDC%20Test%20Simple%20API.postman_collection.json) file to try to access the above endpoint and run some API Test.

## Demo
All the API functions can be tried on this demo URL
```bash
https://jdc.ranggasenjari.my.id
```
