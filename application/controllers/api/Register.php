<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Register extends RestController {

    function __construct()
    {
        parent::__construct();
    }

    public function index_post()
    {
        $username = $this->post('username');
        $role         = $this->post('role');
        $password = random_password();
        $email       = $username.'@domain.com';

        $create_role = $this->ion_auth->create_group($role);
        if (!$create_role ){
           $group_id = $this->db->where('name', $role)->get('groups')->row()->id;
        } else {
           $group_id = $create_role;
        }
        $additional_data = array(
           'first_name' => $username
        );
        $group = [$group_id];
        $reg = $this->ion_auth->register($username, $password, $email, $additional_data, $group);
        
        if (!$reg){
           $this->response( 'failed', 404 );
        } else {
           $res = [
                'username' => $username,
                'role' => $role,
                'password' => $password,
           ];
           $this->response( $res, 200 );
        }
    }
    
}