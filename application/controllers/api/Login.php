<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use Firebase\JWT\JWT;
use Carbon\Carbon;

class Login extends RestController {

    function __construct()
    {
        parent::__construct();
    }

    public function index_post()
    {
        $username= $this->post('username');
        $password = $this->post('password');
        $remember = TRUE;
        if ($this->ion_auth->login($username, $password, $remember)){
            $id = $this->ion_auth->user()->row()->id;
            $key = config_item('jwt_key');
            $date = Carbon::now(new DateTimeZone('Asia/Jakarta'));
            $payload = [
                'id'    => $id,
                'username' => $username,
                'iat'   => (int)$date->format('U'),
                'exp'   => (int)$date->addHour()->format('U')
            ];
            $res= [
                'id' => $id,
                'username' => $username,
                'accessToken' => JWT::encode($payload, $key, 'HS256')
            ];
            $this->response($res, 200);
        } else {
            $this->response('failed', 404);
        }
    }

}