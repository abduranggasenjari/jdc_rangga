<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Carbon\Carbon;

class App extends RestController {
    
    function __construct()
    {
        parent::__construct();
        $headers = $this->input->request_headers();
        $this->validation = $this->_headerVerification($headers);
        if (!$this->validation){
            $this->response('Unauthorized', 401);
            die();
        }
    }

    public function validasi_get()
    {
        $res = [
            'is_valid' => true,
            'expired_at' => Carbon::parse($this->validation->exp)->format('Y-m-d H:i:s'),
            'username' => $this->validation->username
        ];
        $this->response($res, 200);
    }

    function _headerVerification($headers)
    {
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            try {
                $decoded = JWT::decode(str_replace('Bearer ', '', $headers['Authorization']), new Key(config_item('jwt_key'), 'HS256')) ?? false;
                if ($decoded != false) {
                    return $decoded;
                }
            } catch (Exception $e){
                return false;
            }
		} else {
            return false;
		}
    }
}