<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title><?= config_item('judul') ?> - Masuk</title>
		<meta name="keywords" content="<?= config_item('judul') ?>" />
		<meta name="description" content="<?= config_item('judul') ?>">
		<meta name="author" content="Dinas Komunikasi dan Informatika Kabupaten Langkat">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?=config_item('aset')?>vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?=config_item('aset')?>vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?=config_item('aset')?>vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?=config_item('aset')?>vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?=config_item('aset')?>stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?=config_item('aset')?>stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?=config_item('aset')?>stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<?=config_item('aset')?>vendor/modernizr/modernizr.js"></script>

	</head>
	<body style='background-image: url("<?=config_item('aset')?>images/iWBk7rH.jpg");background-repeat: no-repeat;background-position: center center;background-size: cover;'>
		<!-- start: page -->
		<section class="body">

			
				<section role="main" class="content">
					<!-- start: page -->
					<div class="row pt-xlg">
					<div class="col-md-4 col-md-offset-2">
			<section class="body-sign" style="max-width: 5000px;" >				
			<div class="center-sign" style="color: #1b3a66;"> 

<!-- <p class="mb-lg">
<img src="<?=config_item('aset')?>images/kulthuml.png" height="88" class="img-responsive" alt="Porto Admin" /> 
</p> -->
<h3 class="mt-none"><strong>HALAMAN PENGELOLA</strong></h3>
<h4 class="mt-none"><strong>LAYANAN PENGADUAN ONLINE</strong></h4>
<h5 class="mt-none">Dinas Komunikasi dan Informatika Kabupaten Langkat</h5>
<p class="mt-xlg text-justify" style="color: #4d423c;"><strong>Layanan ini dipersembahkan oleh Diskominfo Kabupaten Langkat untuk menampung pengaduan dan permohonan informasi terkait layanan di Dinas Komunikasi dan Informatika Kabupaten Langkat. Segala keluhan dan pertanyaan terkait penggunaan layanan ini dapat disampaikan ke Diskominfo Langkat</strong></p>
							
				</div>
				</section>
					
					</div>
					<div class="col-md-4">
			<section class="body-sign">				
			<div class="center-sign"> 
			
									

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Masuk</h2>
					</div>
					<div class="panel-body" style='background: #fff4e4;'>
					
					<?php echo form_open("auth/login");?>
							<div class="form-group mb-lg">
								<label>Nama Pengguna</label>
								<div class="input-group input-group-icon">
									<input name="identity" type="text" class="form-control input-lg" autofocus/>
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Kata sandi</label>
									<a href="pages-recover-password.html" class="pull-right">Lupa Sandi?</a>
								</div>
								<div class="input-group input-group-icon">
									<input name="password" type="password" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="remember" name="remember" type="checkbox"/>
										<label for="RememberMe">Ingat Saya</label>
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary hidden-xs">Masuk</button>
									<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Masuk</button>
								</div>
							</div>

							

							<br/>
							<br/>

							<p class="text-center small">Hubungi Admin <a href="#">Diskominfo Langkat </a> untuk mendapatkan Akun.

							<?php echo form_close();?>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md" style="color: #712e2e !important;">&copy; 2022. Dinas Komunikasi dan Informatika Kab. Langkat.</p>
			</div>
			
			</section>
					
					</div>
					</div>

					<!-- end: page -->
				</section>

		</section>

		<!-- Vendor -->
		<script src="<?=config_item('aset')?>vendor/jquery/jquery.js"></script>
		<script src="<?=config_item('aset')?>vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?=config_item('aset')?>vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?=config_item('aset')?>vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?=config_item('aset')?>vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?=config_item('aset')?>vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?=config_item('aset')?>vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?=config_item('aset')?>javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?=config_item('aset')?>javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?=config_item('aset')?>javascripts/theme.init.js"></script>

	</body>
</html>