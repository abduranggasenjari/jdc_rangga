<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('random_password'))
{
 function random_password() 
{
    $alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890';
    $password = array(); 
    $alpha_length = strlen($alphabet) - 1; 
    for ($i = 0; $i < 6; $i++) 
    {
        $n = rand(0, $alpha_length);
        $password[] = $alphabet[$n];
    }
    return implode($password); 
}
}
